package tdd.fizzbuzz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {
    @Test
    void should_say_Fizz_when_countOff_given_a_number_is_multiple_of_3() {
        //given
        int number = 3;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals("Fizz", wordSay);

    }

    @Test
    void should_say_Fuzz_when_countOff_given_a_number_is_multiple_of_5() {
        //given
        int number = 5;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals("Buzz", wordSay);

    }

    @Test
    void should_say_Fuzz_when_countOff_given_a_number_is_multiple_of_3_and_5() {
        //given
        int number = 15;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals("FizzBuzz", wordSay);

    }

    @Test
    void should_say_Fuzz_when_countOff_given_a_number_is_multiple_of_7() {
        //given
        int number = 7;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals("Whizz", wordSay);

    }

    @Test
    void should_say_Fuzz_when_countOff_given_a_number_is_multiple_of_3_and_7() {
        //given
        int number = 21;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals("FizzWhizz", wordSay);

    }

    @Test
    void should_say_Fuzz_when_countOff_given_a_number_is_multiple_of_5_and_7() {
        //given
        int number = 35;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals("BuzzWhizz", wordSay);

    }

    @Test
    void should_say_Fuzz_when_countOff_given_a_number_is_multiple_of_3_and_5_and_7() {
        //given
        int number = 105;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals("FizzBuzzWhizz", wordSay);

    }

    @Test
    void should_say_Fuzz_when_countOff_given_a_normal_number() {
        //given
        int number = 1;

        //when
        String wordSay = new FizzBuzz().countOff(number);

        //then
        assertEquals(String.valueOf(number), wordSay);

    }
}
